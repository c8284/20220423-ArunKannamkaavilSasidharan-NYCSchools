package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data

data class Schools(
    var dbn: String,
    var school_name: String,
    var website: String,
    var phone_number: String,
    var primary_address_line_1: String,
    var city: String,
    var zip: String,
    var state_code: String,
    var overview_paragraph: String
)