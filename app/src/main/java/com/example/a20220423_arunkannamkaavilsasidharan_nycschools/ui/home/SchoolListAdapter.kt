package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.Schools
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.databinding.SchoolListItemBinding

/**
 * Adapter class for school list recyclerview
 *
 */
class SchoolListAdapter(
    private val schoolItemClickListener: SchoolItemClickListener
) : RecyclerView.Adapter<SchoolListAdapter.ViewHolder>() {

    private var mutableSchoolList = mutableListOf<Schools?>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            SchoolListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.bind(position)
    }

    override fun getItemCount(): Int = mutableSchoolList.size

    /**
     * View holder class for school list recyclerview
     */
    inner class ViewHolder(binding: SchoolListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val schoolNameView: TextView = binding.schoolName
        val schoolWebsiteView: TextView = binding.schoolWebsite
        val schoolAddrView: TextView = binding.schoolAddress
        val schoolPhView: TextView = binding.schoolPh
        val schoolCardView: CardView = binding.schoolItemCard

        fun bind(position: Int) {
            val schoolItem = mutableSchoolList[position]!!
            schoolNameView.text = schoolItem.school_name
            schoolWebsiteView.text = schoolItem.website
            schoolAddrView.text = getSchoolAddr(schoolItem)
            schoolPhView.text = schoolItem.phone_number
            schoolCardView.setOnClickListener {
                schoolItemClickListener.onSchoolItemClicked(position)
            }
        }

        private fun getSchoolAddr(schoolItem: Schools): String {
            return schoolItem.primary_address_line_1 + ", " + schoolItem.city + ", " + schoolItem.state_code + ", " + schoolItem.zip
        }
    }

    /**
     * to set the school list data  and to notify when school list got refresh
     * @param schoolList - school list to update the recycelverview
     */
    fun setSchoolListData(schoolList: List<Schools>) {
        mutableSchoolList.clear()
        mutableSchoolList.addAll(schoolList)
        notifyDataSetChanged()
    }

    /**
     *  interface for school item clicks
     */
    interface SchoolItemClickListener {
        fun onSchoolItemClicked(position: Int)
    }
}