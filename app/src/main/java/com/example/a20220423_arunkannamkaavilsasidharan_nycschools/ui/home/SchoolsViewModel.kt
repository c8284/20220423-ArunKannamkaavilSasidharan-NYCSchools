package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api.ApiHelper
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common.Resource
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.Schools
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

/**
 * view model class for Schools list
 */
class SchoolsViewModel(private val apiHelper: ApiHelper) : ViewModel() {

    private val schoolsLiveData = MutableLiveData<Resource<List<Schools>>>()
    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        schoolsLiveData.postValue(Resource.failure("Something Went Wrong", null))
    }

    init {
        fetchSchoolList()
    }

    fun fetchSchoolList() {
        viewModelScope.launch(exceptionHandler) {
            schoolsLiveData.postValue(Resource.loading(null))
            try {
                val schoolListApi = apiHelper.getSchoolList()
                schoolsLiveData.postValue(Resource.success(schoolListApi))
            } catch (e: Exception) {
                schoolsLiveData.postValue(Resource.failure(e.toString(), null))
            }

        }
    }

    fun getSchoolList(): LiveData<Resource<List<Schools>>> {
        return schoolsLiveData
    }


}