package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.home


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.R
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.databinding.ActivityMainBinding

/**
 * Home screen of the NYC app
 */
class SchoolHomeScreen : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState == null) {
            val isTablet = this.resources.getBoolean(R.bool.isTablet)
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SchoolListFragment())
                .commit()
            if (isTablet) {
//                supportFragmentManager.beginTransaction()
//                    .replace(R.id.school_detail_container, SchoolDetailsFragment.newInstance(0))
//                    .commit()

            }
        }

    }
}