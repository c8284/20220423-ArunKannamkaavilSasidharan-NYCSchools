package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.R
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api.ApiHelperImpl
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api.ServiceBuilder
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common.LoadingState
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common.ViewModelFactory
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.databinding.FragmentSchoolListBinding
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.schoolDetail.SchoolDetailsFragment


/**
 * A fragment representing a list of schools.
 */
class SchoolListFragment : Fragment(R.layout.fragment_school_list),
    SchoolListAdapter.SchoolItemClickListener {

    private var _binding: FragmentSchoolListBinding? = null
    private val binding get() = _binding!!
    private lateinit var schoolListAdapter: SchoolListAdapter
    private lateinit var schoolsViewModel: SchoolsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        schoolsViewModel = ViewModelProvider(
            requireActivity(),
            ViewModelFactory(ApiHelperImpl(ServiceBuilder.apiService))
        ).get(SchoolsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSchoolListBinding.inflate(inflater, container, false)
        schoolListAdapter = SchoolListAdapter(this)
        binding.swipeContainer.setOnRefreshListener(OnRefreshListener {
            schoolsViewModel.fetchSchoolList()
        })

        binding.recyclerViewSchoolData.apply {
            binding.recyclerViewSchoolData.layoutManager = LinearLayoutManager(requireContext())

            adapter = schoolListAdapter
            hasFixedSize()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchSchoolData()
    }

    /**
     * fetch school list from the api call
     */
    private fun fetchSchoolData() {
        binding.errorView.visibility = View.GONE
        schoolsViewModel.getSchoolList().observe(viewLifecycleOwner) {
            when (it.status) {
                LoadingState.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    binding.swipeContainer.isRefreshing = false
                    it.data?.let { schoolList -> schoolListAdapter.setSchoolListData(schoolList) }
                }
                LoadingState.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                LoadingState.FAILURE -> {
                    binding.progressBar.visibility = View.GONE
                    binding.errorView.visibility = View.VISIBLE
                    binding.swipeContainer.isRefreshing = false
                }
            }
        }
    }

    /**
     * implementation of on school item click from the list of schools
     */
    override fun onSchoolItemClicked(position: Int) {

        val isTablet = requireActivity().resources.getBoolean(R.bool.isTablet)
        if (isTablet) {
//            activity?.supportFragmentManager?.beginTransaction()
//                ?.replace(R.id.school_detail_container, SchoolDetailsFragment.newInstance(position))
//                ?.commit()
        } else {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.container, SchoolDetailsFragment.newInstance(position))
                ?.addToBackStack(null)
                ?.commit()
        }
    }
}