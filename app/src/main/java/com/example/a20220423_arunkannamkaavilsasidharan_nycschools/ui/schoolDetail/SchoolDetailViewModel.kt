package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.schoolDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api.ApiHelper
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common.Resource
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.SchoolDetails
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * view model class for Schools data of marks
 */
class SchoolDetailViewModel(private val apiHelper: ApiHelper) : ViewModel() {
    private val schoolDetailsLiveData = MutableLiveData<Resource<List<SchoolDetails>>>()

    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        schoolDetailsLiveData.postValue(Resource.failure("Something Went Wrong", null))
    }


     fun fetchSchoolDetails(param: String) {
        viewModelScope.launch(exceptionHandler) {
            schoolDetailsLiveData.postValue(Resource.loading(null))
            try {
                var schoolListApi:List<SchoolDetails>?  = null
                withContext(Dispatchers.IO){
                 schoolListApi = apiHelper.getSchoolDetails(param)

            }

                schoolDetailsLiveData.postValue(Resource.success(schoolListApi))
            } catch (e: Exception) {
                schoolDetailsLiveData.postValue(Resource.failure(e.toString(), null))
            }

        }
    }

    fun getSchoolDetails(): LiveData<Resource<List<SchoolDetails>>> {
        return schoolDetailsLiveData
    }

}
