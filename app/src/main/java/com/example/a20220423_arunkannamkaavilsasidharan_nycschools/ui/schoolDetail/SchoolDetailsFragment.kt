package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.schoolDetail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.R
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api.ApiHelperImpl
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api.ServiceBuilder
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common.LoadingState
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common.ViewModelFactory
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.SchoolDetails
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.Schools
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.databinding.SchoolDetailsBinding
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.home.SchoolsViewModel

/**
 * A fragment representing details of school
 */
class SchoolDetailsFragment : Fragment(R.layout.school_details) {

    private lateinit var schoolData: Schools
    private var _binding: SchoolDetailsBinding? = null
    private val binding get() = _binding!!
    private lateinit var schoolDetailViewModel: SchoolDetailViewModel
    private lateinit var schoolsViewModel: SchoolsViewModel

    companion object {
        fun newInstance(position: Int): SchoolDetailsFragment {
            val fragment = SchoolDetailsFragment()
            val args = Bundle()
            args.putInt("position", position)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var position = requireArguments().getInt("position", 0)

        schoolsViewModel = ViewModelProvider(
            requireActivity(),
            ViewModelFactory(ApiHelperImpl(ServiceBuilder.apiService))
        ).get(SchoolsViewModel::class.java)
        schoolDetailViewModel = ViewModelProvider(
            requireActivity(),
            ViewModelFactory(ApiHelperImpl(ServiceBuilder.apiService))
        ).get(SchoolDetailViewModel::class.java)

        schoolsViewModel.getSchoolList().observe(this) {
            schoolData = it.data?.let { schoolList -> schoolList[position] }!!
            schoolDetailViewModel.fetchSchoolDetails(schoolData.dbn)
            getSchoolData(schoolData)
        }

    }

    private fun getSchoolData(schoolData: Schools) {
        schoolDetailViewModel.getSchoolDetails().observe(viewLifecycleOwner) { it ->
            when (it.status) {
                LoadingState.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                LoadingState.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    it.data?.let {
                        loadSchoolDetails(it, schoolData)
                    }
                }
                LoadingState.FAILURE -> {
                    binding.progressBar.visibility = View.GONE
                    binding.errorView.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun loadSchoolDetails(data: List<SchoolDetails>, schoolData: Schools) {
        try {
            binding.schoolTitle.text = data[0].school_name
            binding.schoolDesc.text = schoolData.overview_paragraph
            binding.schoolTestTakers.text = data[0].num_of_sat_test_takers
            binding.schoolMathScore.text = data[0].sat_math_avg_score
            binding.schoolReadScore.text = data[0].sat_critical_reading_avg_score
            binding.schoolWriteScore.text = data[0].sat_writing_avg_score
        } catch (e: Exception) {
            binding.errorView.visibility = View.VISIBLE
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SchoolDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

}