package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common

/**
 * Enum: Loading state of Api service  call
 *  LOADING - in progress of Api call
 *  SUCCESS - on success of Api call
 *  FAILURE - on failure of Api call
 *
 */
enum class LoadingState {
    LOADING,
    SUCCESS,
    FAILURE
}