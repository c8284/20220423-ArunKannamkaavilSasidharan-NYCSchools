package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api.ApiHelper
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.home.SchoolsViewModel
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.schoolDetail.SchoolDetailViewModel

class ViewModelFactory(private val apiHelper: ApiHelper) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SchoolsViewModel::class.java)) {
            return SchoolsViewModel(apiHelper) as T
        }
        if (modelClass.isAssignableFrom(SchoolDetailViewModel::class.java)) {
            return SchoolDetailViewModel(apiHelper) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}