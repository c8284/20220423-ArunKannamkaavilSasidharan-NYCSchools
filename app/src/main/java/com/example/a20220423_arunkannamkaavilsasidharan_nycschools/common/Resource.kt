package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common

data class Resource<out T>(val status: LoadingState, val data: T?, val message: String?) {

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(LoadingState.SUCCESS, data, null)
        }

        fun <T> failure(msg: String, data: T?): Resource<T> {
            return Resource(LoadingState.FAILURE, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(LoadingState.LOADING, data, null)
        }

    }

}