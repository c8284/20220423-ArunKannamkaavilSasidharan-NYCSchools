package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api

/**
 * Api URL details
 *
 */
object ApiConstant {
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
}