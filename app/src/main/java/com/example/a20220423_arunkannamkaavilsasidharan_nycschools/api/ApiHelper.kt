package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api

import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.SchoolDetails
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.Schools

/**
 * interface for Api service call
 *
 */
interface ApiHelper {
    suspend fun getSchoolList(): List<Schools>
    suspend fun getSchoolDetails(dbn: String): List<SchoolDetails>
}