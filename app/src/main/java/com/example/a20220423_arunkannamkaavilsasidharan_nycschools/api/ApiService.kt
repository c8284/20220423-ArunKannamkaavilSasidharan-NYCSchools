package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api

import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.SchoolDetails
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.Schools
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Api service call interface
 *
 */
interface ApiService {

    @GET("s3k6-pzi2")
    suspend fun getNYCSchoolList(): List<Schools>

    @GET("f9bf-2cp4")
    suspend fun getNYCSchoolDetails(@Query("dbn") param: String): List<SchoolDetails>

}