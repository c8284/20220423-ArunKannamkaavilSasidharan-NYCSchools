package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api


/**
 * implementation class for Api service call
 *
 */
class ApiHelperImpl(private val apiService: ApiService) : ApiHelper {
    override suspend fun getSchoolList() = apiService.getNYCSchoolList()
    override suspend fun getSchoolDetails(dbn: String) = apiService.getNYCSchoolDetails(dbn)
}