package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.schoolDetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.TestCoroutineRule
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api.ApiHelper
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common.Resource
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.SchoolDetails
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.schoolDetail.SchoolDetailViewModel
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SchoolDetailViewModelTest {

    private lateinit var viewModel: SchoolDetailViewModel

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var apiHelper: ApiHelper

    @Mock
    private lateinit var apiSchoolDetailsObserver: Observer<Resource<List<SchoolDetails>>>

    @Before
    fun setUp() {

    }

//    lateinit var mockRepo: MockRepo

    @Test
    fun givenResp200_whenFetch_returnSuccess() {

        testCoroutineRule.runBlockingTest {
//            mockRepo.fetchMockSchoolDetails()
            Mockito.doReturn(emptyList<SchoolDetails>())
                .`when`(apiHelper)
                .getSchoolDetails("21K344")
            viewModel = SchoolDetailViewModel(apiHelper)
            viewModel.fetchSchoolDetails("21K344")
            viewModel.getSchoolDetails().observeForever(apiSchoolDetailsObserver)
            Mockito.verify(apiHelper).getSchoolDetails("21K344")
            Mockito.verify(apiSchoolDetailsObserver).onChanged(Resource.success(emptyList()))

        }
    }

    @Test
    fun givenRespError_whenFetch_returnError() {
        testCoroutineRule.runBlockingTest {
            val errorMessage = "Something went wrong"
            Mockito.doThrow(RuntimeException(errorMessage))
                .`when`(apiHelper)
                .getSchoolDetails("")
            viewModel = SchoolDetailViewModel(apiHelper)
            viewModel.fetchSchoolDetails("")
            viewModel.getSchoolDetails().observeForever(apiSchoolDetailsObserver)
            Mockito.verify(apiHelper).getSchoolDetails("")
            Mockito.verify(apiSchoolDetailsObserver).onChanged(
                Resource.failure(
                    RuntimeException(errorMessage).toString(),
                    null
                )
            )
        }
    }

    @After
    fun tearDown() {
        viewModel.getSchoolDetails().removeObserver(apiSchoolDetailsObserver)
    }

}