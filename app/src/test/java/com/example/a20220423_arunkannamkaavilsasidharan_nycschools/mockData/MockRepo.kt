package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.mockData

import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.SchoolDetails

class MockRepo {
    fun fetchMockSchoolDetails(): List<SchoolDetails> {
        return listOf(
            SchoolDetails("31R080", "THE MICHAEL J. PETRIDES SCHOOL", "107", "147", "176", "17")
        )
    }
}