package com.example.a20220423_arunkannamkaavilsasidharan_nycschools.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.TestCoroutineRule
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.api.ApiHelper
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.common.Resource
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.data.Schools
import com.example.a20220423_arunkannamkaavilsasidharan_nycschools.ui.home.SchoolsViewModel
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SchoolsViewModelTest {

    private lateinit var viewModel: SchoolsViewModel

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var apiHelper: ApiHelper

    @Mock
    private lateinit var apiSchoolListObserver: Observer<Resource<List<Schools>>>

    @Before
    fun setUp() {

    }

    @Test
    fun givenResp200_whenFetch_returnSuccess() {

        testCoroutineRule.runBlockingTest {
            Mockito.doReturn(emptyList<Schools>())
                .`when`(apiHelper)
                .getSchoolList()
            viewModel = SchoolsViewModel(apiHelper)
            viewModel.getSchoolList().observeForever(apiSchoolListObserver)
            Mockito.verify(apiHelper).getSchoolList()
            Mockito.verify(apiSchoolListObserver).onChanged(Resource.success(emptyList()))
        }
    }

    @Test
    fun givenRespError_whenFetch_returnError() {
        testCoroutineRule.runBlockingTest {
            val errorMessage = "Error Message For You"
            Mockito.doThrow(RuntimeException(errorMessage))
                .`when`(apiHelper)
                .getSchoolList()
            viewModel = SchoolsViewModel(apiHelper)
            viewModel.getSchoolList().observeForever(apiSchoolListObserver)
            Mockito.verify(apiHelper).getSchoolList()
            Mockito.verify(apiSchoolListObserver).onChanged(
                Resource.failure(
                    RuntimeException(errorMessage).toString(),
                    null
                )
            )

        }
    }

    @After
    fun tearDown() {
        viewModel.getSchoolList().removeObserver(apiSchoolListObserver)
    }

}